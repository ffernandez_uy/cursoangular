import {
    reducer,
    initializeDestinosViajesState,
    INIT_MY_DATA,
    NUEVO_DESTINO
} from './destinos-viajes-state.model';

import { DestinoViaje } from './destino-viaje.model';
import { Action, select } from '@ngrx/store';

describe('reducerDestinosViajes', () => {
    it('should reduce init data', () => {
        //setup
        const prevState =  initializeDestinosViajesState;
//const destinoPrueba: string[] = ["destino 1", "destino 2"];
        const action: Action = INIT_MY_DATA({ destinos: ["destino 1", "destino 2"]});
        //action
        const newState = reducer(prevState, action);
        //assertions
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].n).toEqual('destino 1');
        //tear Down
    });

    it('should reduce new item added', () => {
        //setup
        const prevState = initializeDestinosViajesState;
       // const destinoPrueba: DestinoViaje = new DestinoViaje('barcelona', 'url');
        const action: Action = NUEVO_DESTINO({ destino: new DestinoViaje('barcelona', 'url') });
       //action 
        const newState = reducer(prevState, action);
        //assertions
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].n).toEqual('barcelona');
        //tear Down
    })
})
 