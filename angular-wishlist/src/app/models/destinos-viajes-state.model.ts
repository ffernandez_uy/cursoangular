import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { DestinoViaje } from './destino-viaje.model';
import { HttpClientModule } from '@angular/common/http';
// import { type } from 'os';


//Estado
export interface DestinosViajesState { 
  items: DestinoViaje[];
  loading: boolean;
  favorito: DestinoViaje;
}

export function intializeDestinosViajesState () {
  return {
    items: [],
    loading: false,
    favorito: null
  };
};

//Acciones
export enum DestinosViajesActionType { 
  NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
  ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
  VOTE_UP = '[Destinos Viajes] Vote Up',
  VOTE_DOWN = '[Destinos Viajes] Vote Down',
  INIT_MY_DATA = '[Destinos Viajes] Init my Data'
}

export class NuevoDestinoAction implements Action { 
  type = DestinosViajesActionType.NUEVO_DESTINO;
  constructor(public destino: DestinoViaje) { }
}

export class ElegidoFavoritoAction implements Action { 
  type = DestinosViajesActionType.ELEGIDO_FAVORITO;
  constructor(public destino: DestinoViaje) { }
}

export class VoteUpAction implements Action { 
  type = DestinosViajesActionType.VOTE_UP;
  static destino: DestinoViaje;
  constructor(public destino: DestinoViaje) { }
}

export class VoteDownAction implements Action { 
  type = DestinosViajesActionType.VOTE_DOWN;
  static destino: DestinoViaje;
  constructor(public destino: DestinoViaje) { }
}

export class InitMyDataAction implements Action {
  type = DestinosViajesActionType.INIT_MY_DATA;
  constructor(public destinos: string[]) { }
}

export type DestinosViajesActions = NuevoDestinoAction | ElegidoFavoritoAction
  | VoteUpAction | VoteDownAction | InitMyDataAction;

//Reducers

export function reducerDestinosViajes(
  state: DestinosViajesState,
  action: DestinosViajesActions
): DestinosViajesState { 
  switch (action.type) { 
    case DestinosViajesActionType.NUEVO_DESTINO: { 
      return {
        ...state,
        items: [...state.items, (action as NuevoDestinoAction).destino]
      };
    }
    case DestinosViajesActionType.ELEGIDO_FAVORITO: {
      state.items.forEach(x => x.setSelected(false));
      let fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
      fav.setSelected(true);
      return {
        ...state,
        favorito: fav
      };
    }
    case DestinosViajesActionType.VOTE_UP: {
      const d: DestinoViaje = (action as VoteUpAction).destino;
      d.voteUp();
      return { ...state };
    }  
    case DestinosViajesActionType.VOTE_DOWN: {
      const d: DestinoViaje = (action as VoteDownAction).destino;
      d.voteDown();
      return { ...state };
    }
    case DestinosViajesActionType.INIT_MY_DATA: {
      const destinos: string[] = (action as InitMyDataAction).destinos;
      return {
        ...state,
        items:destinos.map((d) => new DestinoViaje(d, ''))
      };
    } 
      return state;
  }
}

//Effects

@Injectable()
export class DestinosViajesEffects { 
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(DestinosViajesActionType.NUEVO_DESTINO),
    map((action:NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
  );
  
  constructor( private actions$: Actions){ }
}